# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

common_globals = {}
execfile_('common.py', common_globals)

MODEL_VERSIONS = {
    u'T27P': u'45.81.0.110',
    u'T29G': u'46.81.0.110',
    u'T41P': u'36.81.0.110',
    u'T42G': u'29.81.0.110',
    u'T46G': u'28.81.0.110',
    u'T48G': u'35.81.0.110',
    u'W52P': u'25.81.0.60',
}
COMMON_FILES = [
    ('y000000000028.cfg', u'T46-28.81.0.110.rom', 'model.tpl'),
    ('y000000000029.cfg', u'T42-29.81.0.110.rom', 'model.tpl'),
    ('y000000000035.cfg', u'T48-35.81.0.110.rom', 'model.tpl'),
    ('y000000000036.cfg', u'T41-36.81.0.110.rom', 'model.tpl'),
    ('y000000000045.cfg', u'T27-45.81.0.110.rom', 'model.tpl'),
    ('y000000000046.cfg', u'T29-46.81.0.110.rom', 'model.tpl'),
]
COMMON_FILES_DECT = [
    ('y000000000025.cfg', u'25.81.0.60.rom', u'26.81.0.50.rom', 'W52P.tpl'),
]


class YealinkPlugin(common_globals['BaseYealinkPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BaseYealinkPgAssociator'](MODEL_VERSIONS)

    # Yealink plugin specific stuff

    _COMMON_FILES = COMMON_FILES

    def configure_common(self, raw_config):
        super(YealinkPlugin, self).configure_common(raw_config)
        for filename, fw_filename, fw_handset_filename, tpl_filename in COMMON_FILES_DECT:
            tpl = self._tpl_helper.get_template('common/%s' % tpl_filename)
            dst = os.path.join(self._tftpboot_dir, filename)
            raw_config[u'XX_fw_filename'] = fw_filename
            raw_config[u'XX_fw_handset_filename'] = fw_handset_filename
            self._tpl_helper.dump(tpl, raw_config, dst, self._ENCODING)
