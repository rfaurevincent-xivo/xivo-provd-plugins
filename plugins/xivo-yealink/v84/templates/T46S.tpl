{% extends 'base.tpl' -%}

{% block model_specific_parameters -%}
gui_lang.delete = http://localhost/003.GUI.French.lang
programablekey.3.type = 0

phone_setting.backgrounds = xivologo.png
wallpaper_upload.url = http://{{ ip }}:8667/xivologo.png
{% endblock %}
