# -*- coding: utf-8 -*-

# Copyright (C) 2013-2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os

common_globals = {}
execfile_('common.py', common_globals)

MODEL_VERSIONS = {
    u'T31P': u'124.85.0.15',
    u'T33G': u'124.85.0.15',
    u'T53': u'96.85.0.5',
    u'T54W': u'96.85.0.5',
}
COMMON_FILES = [
# voir Yealink SIP IP Phones Auto Provisioning Guide V1.1.pdf dans la page du modèle sur http://support.yealink.com/documentFront/forwardToDocumentFrontDisplayPage
    ('y000000000123.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.85.0.15.rom', 'model.tpl'), #T31P
    ('y000000000124.cfg', u'T31(T30,T30P,T31G,T31P,T33P,T33G)-124.85.0.15.rom', 'model.tpl'), #T33G
    ('y000000000095.cfg', u'T54W(T57W,T53W,T53)-96.85.0.5.rom', 'model.tpl'), #T53
    ('y000000000096.cfg', u'T54W(T57W,T53W,T53)-96.85.0.5.rom', 'model.tpl'), #T54W
]


class YealinkPlugin(common_globals['BaseYealinkPlugin']):
    IS_PLUGIN = True

    pg_associator = common_globals['BaseYealinkPgAssociator'](MODEL_VERSIONS)

    # Yealink plugin specific stuff

    _COMMON_FILES = COMMON_FILES
